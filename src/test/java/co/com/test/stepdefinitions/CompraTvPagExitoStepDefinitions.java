package co.com.test.stepdefinitions;


import co.com.test.model.BusquedaArticulo;
import co.com.test.questions.ElTv;
import co.com.test.tasks.Abrir;
import co.com.test.tasks.AnadirProducto;
import co.com.test.tasks.Buscar;
import cucumber.api.java.Before;
import cucumber.api.java.es.Cuando;
import cucumber.api.java.es.Dado;
import cucumber.api.java.es.Entonces;
import net.serenitybdd.screenplay.actors.OnlineCast;

import java.util.List;

import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static net.serenitybdd.screenplay.actors.OnStage.setTheStage;
import static net.serenitybdd.screenplay.actors.OnStage.theActorCalled;
import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;
import static org.hamcrest.Matchers.equalTo;


public class CompraTvPagExitoStepDefinitions {

    @Before
    public void configuracionInicial(){
        setTheStage(new OnlineCast());
    }

    @Dado("^que un usuario quiere comprar un televisor para ver el mundial de futbol$")
    public void queUnUsuarioQuiereComprarUnTelevisorParaVerElMundialDeFutbol() {
        theActorCalled("usuario")
                .wasAbleTo(Abrir.laPaginaDelExito());


    }

    @Cuando("^ingresa a la pagina virtual del exito y selecciona el televisor que mas le gusta$")
    public void ingresaALaPaginaVirtualDelExitoYSeleccionaElTelevisorQueMasLeGusta(List <BusquedaArticulo> BusquedaArticulo) {
        theActorInTheSpotlight().attemptsTo(Buscar.elTelevisorQueQuieroComprar(BusquedaArticulo));
        //theActorInTheSpotlight().attemptsTo(AnadirProducto.alCarritoDeCompra());

    }

    @Entonces("^realiza la compra para que le sea enviado a su casa\\.$")
    public void realizaLaCompraParaQueLeSeaEnviadoASuCasa() {
        theActorInTheSpotlight().should(seeThat(ElTv.estaEnElCarrito(), equalTo("Tienes 1 producto en el carrito")));



    }

}
