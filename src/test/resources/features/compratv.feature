#Author:stephanie
#language:es

Característica: Compra de tv por pagina web del exito
  Esquema del escenario: usuario realiza compra de tv por pagina web
    Dado que un usuario quiere comprar un televisor para ver el mundial de futbol
    Cuando ingresa a la pagina virtual del exito y selecciona el televisor que mas le gusta
      |articulo  | marca   | resolucion  |
      |<articulo>|<marca>  |<resolucion> |
    Entonces realiza la compra para que le sea enviado a su casa.

    Ejemplos:
      |articulo     |marca    | resolucion  |
      |Televisores  |LG       | UHD-4K      |