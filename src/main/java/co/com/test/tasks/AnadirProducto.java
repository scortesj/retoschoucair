package co.com.test.tasks;

import co.com.test.userinterface.ResultadoTv;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;

public class AnadirProducto implements Task {


    @Override
    public <T extends Actor> void performAs(T actor) {

        actor.attemptsTo(
                Click.on(ResultadoTv.CLICK_BOTON_AGREGAR_AL_CARRITO));
    }


    public static AnadirProducto alCarritoDeCompra() {
        return Tasks.instrumented(AnadirProducto.class);
    }
}
