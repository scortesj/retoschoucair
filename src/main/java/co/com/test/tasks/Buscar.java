package co.com.test.tasks;

import co.com.test.model.BusquedaArticulo;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.actions.Scroll;
import net.serenitybdd.screenplay.waits.WaitUntil;

import java.util.List;

import static co.com.test.userinterface.PaginaExitoHomePage.BARRA_BUSQUEDA;
import static co.com.test.userinterface.PaginaExitoHomePage.BOTON_BUSCAR;
import static co.com.test.userinterface.ResultadoTv.*;
import static net.serenitybdd.screenplay.Tasks.instrumented;

public class Buscar implements Task {

    private List<BusquedaArticulo> ListaArticulos;

   // private WebDriver driver;


    public Buscar(List<BusquedaArticulo> ListaArticulos) {
        super();
        this.ListaArticulos = ListaArticulos;
    }

    public static Buscar elTelevisorQueQuieroComprar(List <BusquedaArticulo> articulo) {
        return instrumented(Buscar.class, articulo);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {

            actor.attemptsTo(
                    Enter.theValue(ListaArticulos.get(0).getArticulo()).into(BARRA_BUSQUEDA),
                    Click.on(BOTON_BUSCAR),
                    Click.on(SELECCIONAR_TIPO_TECNOLOGIA),
                    WaitUntil.angularRequestsHaveFinished(),
                    Scroll.to(SELECCIONAR_MARCA_LG),
                    WaitUntil.angularRequestsHaveFinished(),
                    Click.on(SELECCIONAR_MARCA_LG),
                    WaitUntil.angularRequestsHaveFinished(),
                    Scroll.to(SELECCIONAR_4K_UHD),
                    Click.on(SELECCIONAR_4K_UHD),
                    WaitUntil.angularRequestsHaveFinished(),
                    Scroll.to(CLICK_TV),
                    WaitUntil.angularRequestsHaveFinished(),
                    Click.on(CLICK_TV),
                    WaitUntil.angularRequestsHaveFinished(),
                    Click.on(CLICK_BOTON_AGREGAR_AL_CARRITO),
                    Scroll.to(CLICK_CARRITO_COMPRA),
                  //  js.executeScript("arguments[0].scrollIntoView(false);", Element);
                    //MoveMouse.to(CLICK_CARRITO_COMPRA).andThen(actions -> actions.equals(CLICK_BOTON_AGREGAR_AL_CARRITO)),
                    Click.on(CLICK_CARRITO_COMPRA)
                    );


      /*  BusquedaArticulo.add(DIV_CONTENEDOR.resolveFor(actor).getText());
        for (BusquedaArticulo busquedaArticulo: ListaArticulos) {
            System.out.println(busquedaArticulo);
            System.out.print(DIV_CONTENEDOR.resolveFor(actor).getText());
        }*/


        }

           /* List<WebElement> ListaArticulos = driver.findElements(By.xpath("//div[@class='exito-search-result-3-x-gallery flex flex-row flex-wrap items-stretch bn ph1']"));

            for( WebElement product : ListaArticulos){
                System.out.println(DIV_CONTENEDOR.resolveFor(actor).getText());
                System.out.println("**********************************************");*/

        }







