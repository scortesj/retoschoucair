package co.com.test.tasks;

import co.com.test.userinterface.PaginaExitoHomePage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Open;

public class Abrir implements Task {

    private PaginaExitoHomePage paginaExitoHomePage;

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(Open.browserOn(paginaExitoHomePage));
    }

    public static Abrir laPaginaDelExito() {
        return Tasks.instrumented(Abrir.class);
    }
}
