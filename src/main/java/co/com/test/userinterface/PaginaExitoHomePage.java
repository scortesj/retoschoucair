package co.com.test.userinterface;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import net.thucydides.core.annotations.DefaultUrl;
import org.openqa.selenium.By;


@DefaultUrl("https://www.exito.com/")
public class PaginaExitoHomePage extends PageObject {

    public static final Target BARRA_BUSQUEDA = Target.the("barra para buscar productos").located(By.id("downshift-0-input"));
    public static final Target BOTON_BUSCAR = Target.the("Botón Buscar").located(By.xpath("//span[@class='flex items-center pointer']"));


}
