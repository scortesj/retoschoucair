package co.com.test.userinterface;

import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

public class ResultadoTv {

    public static final Target SELECCIONAR_TIPO_TECNOLOGIA= Target.the("Seleccionar tipo de tecnologia Televisores").located(By.xpath("//div[contains(text(),'Televisores')]"));
    public static final Target SELECCIONAR_MARCA_LG= Target.the("Scroll para seleccionar marca LG").located(By.xpath("//*[@id = 'LG']"));
    public static final Target SELECCIONAR_4K_UHD= Target.the("Seleccionar 4K-UHD").located(By.xpath("//div[@tabindex='0'][text()='4K Uhd']"));
    public static final Target CLICK_TV = Target.the("Click en TV a comprar").located(By.xpath("//img[@class='exito-product-summary-2-x-imageNormal']"));
    public static final Target CLICK_BOTON_AGREGAR_AL_CARRITO = Target.the("Click en botón Carrito de Compras").located(By.xpath("//div[@class='flex items-center justify-center h-100 pv2 ph6 '][1]"));
    public static final Target CLICK_CARRITO_COMPRA = Target.the("Click en botón Carrito de Compras").located(By.xpath("//*[@id=\"header-container\"]/div[2]/div[2]/div/div/div[3]/div/aside/div/div[1]"));
    public static final Target CANTIDAD_PRODUCTO_AGREGADO_AL_CARRITO = Target.the("Muestra la cantidad de tv agregados al carrito").located(By.xpath("//div[@class='exito-minicart-3-x-customSideBarHeader']"));
    public static final Target DIV_CONTENEDOR= Target.the("Div que contiene todos los tv's buscados después de aplicar filtros").located(By.xpath("(//SPAN[@class='exito-vtex-components-2-x-productBrand t-body galleryItem-productBrand exito-vtex-component-nombre-producto'])[3]"));

}


