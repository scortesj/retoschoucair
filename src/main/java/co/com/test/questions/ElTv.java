package co.com.test.questions;

import static co.com.test.userinterface.PaginaExitoHomePage.BOTON_BUSCAR;

import static co.com.test.userinterface.ResultadoTv.CANTIDAD_PRODUCTO_AGREGADO_AL_CARRITO;
import static co.com.test.userinterface.ResultadoTv.DIV_CONTENEDOR;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Text;

public class ElTv implements Question<String> {

    public static ElTv estaEnElCarrito () {
        return new ElTv();
    }

    @Override
    public String answeredBy(Actor actor) {
        return Text.of(CANTIDAD_PRODUCTO_AGREGADO_AL_CARRITO).viewedBy(actor).asString();
    }
}
